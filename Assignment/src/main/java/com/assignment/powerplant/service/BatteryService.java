package com.assignment.powerplant.service;

import com.assignment.powerplant.dto.request.BatteryRangeRequest;
import com.assignment.powerplant.dto.request.BatteryRequest;
import com.assignment.powerplant.dto.response.BatteryResponse;
import com.assignment.powerplant.dto.response.BatteryWithinRangeResponse;

import com.assignment.powerplant.exceptions.PowerPlantmgtExceptions;



public interface BatteryService {

    /**
     * To handle external api related to save all details of the battery into DB
     * @param request
     * @return
     */
    BatteryResponse create(BatteryRequest request) throws PowerPlantmgtExceptions;
    /**
     * To handle external api related to fetch all battery  details with certain range from DB
     * @param request
     * @return
     */
    BatteryWithinRangeResponse fetchBatteryWithinRange(BatteryRangeRequest request) throws PowerPlantmgtExceptions;
}
