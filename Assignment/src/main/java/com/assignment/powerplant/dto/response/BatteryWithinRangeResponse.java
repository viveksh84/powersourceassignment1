package com.assignment.powerplant.dto.response;

import com.assignment.powerplant.dto.request.QueryBatteryRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class BatteryWithinRangeResponse {

    private QueryBatteryRequest queryBatteryWithinRangeResponse;
    private Long totalBatteryWattCapacity;
    private Double averageBatteryWattCapacity;

    private ResponseHeaderEntity responseHeader;

}
