package com.assignment.powerplant.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.time.LocalDateTime;


@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RequestHeaderEntity {


    private String requestId;

    private LocalDateTime timestamp;


}
