package com.assignment.powerplant.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseHeaderEntity {

	private String requestId;

	private LocalDateTime timestamp;

	private String responseCode;

	private String responseDesc;

	private String responseDescDisplay;
}
