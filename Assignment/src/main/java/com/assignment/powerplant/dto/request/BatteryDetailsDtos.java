package com.assignment.powerplant.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BatteryDetailsDtos {

    @NotBlank(message = "Battery name is required")

    public String name;
    @NotNull(message = "Postal code name is required")

    public String postCode;
    @NotNull(message = "Capacity is required")

    public Long capacity;
}
