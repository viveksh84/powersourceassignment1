package com.assignment.powerplant.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QueryBatteryResponse {

    private String message;
    private boolean dataSaveStatus;


}
