package com.assignment.powerplant.dto.request;

import com.assignment.powerplant.entities.BatteryEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class QueryBatteryRequest {

    List<BatteryEntity> batteryEntities;

}
