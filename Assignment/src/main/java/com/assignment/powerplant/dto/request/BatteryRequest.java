package com.assignment.powerplant.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class BatteryRequest {

    private QueryBatteryRequest queryBatteryRequest;

    private RequestHeaderEntity requestHeader;

}
