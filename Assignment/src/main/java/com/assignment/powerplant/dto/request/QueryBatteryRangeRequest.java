package com.assignment.powerplant.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class QueryBatteryRangeRequest {

    @NotNull(message = "PostCode Start range is required")
    private String postCodeStart;
    @NotEmpty(message = "PostCode End range is required")
    private String postCodeEnd;

}
