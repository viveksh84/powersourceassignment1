package com.assignment.powerplant.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class BatteryResponse {

    private QueryBatteryResponse queryBatteryResponse;

    private ResponseHeaderEntity responseHeader;

}
