package com.assignment.powerplant.entities;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "battery_details")
@Setter
@Getter
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BatteryEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @NotBlank(message = "Battery name is required")
    @Column(name= "battery_name")
    public String name;
    @NotNull(message = "Postal code name is required")
    @Column(name= "postal_code")
    public String postCode;
    @NotNull(message = "Capacity is required")
    @Column(name= "capacity")
    public Long capacity;

}
