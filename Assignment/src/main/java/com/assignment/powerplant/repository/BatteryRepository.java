package com.assignment.powerplant.repository;

import com.assignment.powerplant.entities.BatteryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BatteryRepository extends JpaRepository<BatteryEntity,Long> {

   List<BatteryEntity> findByPostCodeBetweenOrderByNameAsc(String postCodeStart, String postCodeEnd);
}
