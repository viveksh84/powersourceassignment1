package com.assignment.powerplant.serviceimpl;

import com.assignment.powerplant.dto.request.BatteryRangeRequest;
import com.assignment.powerplant.dto.request.BatteryRequest;
import com.assignment.powerplant.dto.request.QueryBatteryRequest;
import com.assignment.powerplant.dto.response.BatteryResponse;
import com.assignment.powerplant.dto.response.BatteryWithinRangeResponse;
import com.assignment.powerplant.dto.response.QueryBatteryResponse;
import com.assignment.powerplant.dto.response.ResponseHeaderEntity;
import com.assignment.powerplant.entities.BatteryEntity;
import com.assignment.powerplant.exceptions.PowerPlantmgtExceptions;
import com.assignment.powerplant.repository.BatteryRepository;
import com.assignment.powerplant.service.BatteryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BatteryServiceImpl implements BatteryService {

    @Autowired
    BatteryRepository batteryRepository;



    List<BatteryEntity> batteryEntities = new ArrayList<>();
    @Override
    public BatteryResponse create(BatteryRequest request) throws PowerPlantmgtExceptions {
        BatteryResponse batteryResponse = new BatteryResponse();
        QueryBatteryResponse queryBatteryResponse = new QueryBatteryResponse();



        try{
            batteryEntities = batteryRepository.saveAll(request.getQueryBatteryRequest().getBatteryEntities());
            if(!batteryEntities.isEmpty()) {
                queryBatteryResponse.setMessage("All data save successfully");
                queryBatteryResponse.setDataSaveStatus(true);

            }else {
                queryBatteryResponse.setDataSaveStatus(false);
                queryBatteryResponse.setMessage("Error in saving data to DB");
            }
            batteryResponse.setQueryBatteryResponse(queryBatteryResponse);
            batteryResponse.setResponseHeader(createResponseHeader(
                    batteryResponse.getQueryBatteryResponse().getMessage(), batteryResponse.getQueryBatteryResponse().isDataSaveStatus()));
        } catch (DataIntegrityViolationException e ) {

            if ( ("battery." + "Duplicate").equals((( org.hibernate.exception.ConstraintViolationException ) e.getCause()).getConstraintName()) ) {

                throw new PowerPlantmgtExceptions("", "");
            }


            throw new PowerPlantmgtExceptions("Constraint violation for save request", "");
        } catch (Exception e){



                throw new PowerPlantmgtExceptions("Unable to acquire JDBC Connection ", "");
        }

        return batteryResponse;
    }

    @Override
    public BatteryWithinRangeResponse fetchBatteryWithinRange(BatteryRangeRequest request) throws PowerPlantmgtExceptions {

        BatteryWithinRangeResponse batteryWithinRangeResponse = new BatteryWithinRangeResponse();
        QueryBatteryRequest queryBatteryWithinRangeResponse = new QueryBatteryRequest();
        try{
            batteryEntities = batteryRepository.findByPostCodeBetweenOrderByNameAsc(
                    request.getQueryBatteryRangeRequest().getPostCodeStart(), request.getQueryBatteryRangeRequest().getPostCodeEnd());
            if(!batteryEntities.isEmpty()) {
                // get the total battery watt capacity
              Long totalBatteryWattCapacity =  batteryEntities.stream().collect(Collectors.summingLong(BatteryEntity::getCapacity));
              // get the average battery watt capacity
              Double averageBatteryWattCapacity = batteryEntities.stream().collect(Collectors.averagingLong(BatteryEntity::getCapacity));
               queryBatteryWithinRangeResponse.setBatteryEntities(batteryEntities);
                batteryWithinRangeResponse.setTotalBatteryWattCapacity(totalBatteryWattCapacity);
                batteryWithinRangeResponse.setAverageBatteryWattCapacity(averageBatteryWattCapacity);
                batteryWithinRangeResponse.setQueryBatteryWithinRangeResponse(queryBatteryWithinRangeResponse);
                batteryWithinRangeResponse.setResponseHeader(createResponseHeader("Data successfully retrived"));

            }else {

               queryBatteryWithinRangeResponse.setBatteryEntities(batteryEntities);
                batteryWithinRangeResponse.setQueryBatteryWithinRangeResponse(queryBatteryWithinRangeResponse);
                batteryWithinRangeResponse.setResponseHeader(createResponseHeader(
                        "No data found in DB",false));
            }
        } catch (DataIntegrityViolationException e ) {

            if ( ("battery." + "Duplicate").equals((( org.hibernate.exception.ConstraintViolationException ) e.getCause()).getConstraintName()) ) {
                // logger.error("There is s duplicate data entry of data ", e);
                throw new PowerPlantmgtExceptions("", "");
            }

            // logger.error("Constraint violation for redeem_data save request {}, {} ", e.getLocalizedMessage(), e);
            throw new PowerPlantmgtExceptions("Constraint violation for save request", "");
        } catch (Exception e){



            throw new PowerPlantmgtExceptions("Unable to acquire JDBC Connection ", "");
        }
        return  batteryWithinRangeResponse;
    }

    public ResponseHeaderEntity createResponseHeader(String response,Boolean saveStatus) {
        ResponseHeaderEntity responseHeaderEntity = new ResponseHeaderEntity();

        responseHeaderEntity.setResponseDescDisplay(response);
        if(saveStatus){
            responseHeaderEntity.setResponseDesc("BAT2000");
            responseHeaderEntity.setResponseCode("200");
        }else{
            responseHeaderEntity.setResponseDesc("BAT4000");
            responseHeaderEntity.setResponseCode("500");
        }

        responseHeaderEntity.setRequestId("1233");

        responseHeaderEntity.setTimestamp(LocalDateTime.now());


        return  responseHeaderEntity;
    }

    public ResponseHeaderEntity createResponseHeader(String response) {
        ResponseHeaderEntity responseHeaderEntity = new ResponseHeaderEntity();

        responseHeaderEntity.setResponseDescDisplay(response);

            responseHeaderEntity.setResponseDesc("BAT2000");
            responseHeaderEntity.setResponseCode("200");


        responseHeaderEntity.setRequestId("1233");

        responseHeaderEntity.setTimestamp(LocalDateTime.now());


        return  responseHeaderEntity;
    }
}
