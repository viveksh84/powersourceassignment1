package com.assignment.powerplant.controller;

import com.assignment.powerplant.dto.request.BatteryRangeRequest;
import com.assignment.powerplant.dto.request.BatteryRequest;
import com.assignment.powerplant.dto.response.BatteryResponse;
import com.assignment.powerplant.dto.response.BatteryWithinRangeResponse;
import com.assignment.powerplant.exceptions.PowerPlantmgtExceptions;
import com.assignment.powerplant.serviceimpl.BatteryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("${base-url.context}")
public class PowerSourceController extends  BaseController{


    @Autowired
    BatteryServiceImpl batteryService;

    /**
     * To handle saving battery list
     * @param request
     * @return
     */
    @PostMapping(value = "/save/battery", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE + "; charset=UTF-8")
    public ResponseEntity<BatteryResponse> saveBatteryDetails( @RequestBody(required = true)  BatteryRequest request,
                                                            HttpServletRequest httpServletRequest) throws PowerPlantmgtExceptions {


        validateIncomingDataRequest(request);
        BatteryResponse batteryResponse = batteryService.create(request);

        return ResponseEntity.status(HttpStatus.CREATED).body(batteryResponse);
    }


    /**
     * To handle query battery list within the range
     * @param request
     * @return
     */
    @PostMapping(value = "/query/battery-range", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE + "; charset=UTF-8")
    public ResponseEntity<BatteryWithinRangeResponse>  queryBatteryDetails(@Valid @RequestBody(required = true) BatteryRangeRequest request,
                                                              HttpServletRequest httpServletRequest) throws PowerPlantmgtExceptions {


        BatteryWithinRangeResponse batteryWithinRangeResponse = batteryService.fetchBatteryWithinRange(request);

        return ResponseEntity.status(HttpStatus.OK).body(batteryWithinRangeResponse);
    }


//    @GetMapping(value = "/hello")
//    public String hello()
//    {
//
//        return "Hello User";
//    }
}
