package com.assignment.powerplant.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus( HttpStatus.BAD_REQUEST )
public class PowerPlantmgtExceptions extends BaseException{

    public PowerPlantmgtExceptions(String message, String code) {
        super(message, code);
    }
}
