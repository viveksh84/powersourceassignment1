package com.assignment.powerplant;

import com.assignment.powerplant.controller.PowerSourceController;
import com.assignment.powerplant.dto.request.*;
import com.assignment.powerplant.dto.response.BatteryResponse;
import com.assignment.powerplant.dto.response.BatteryWithinRangeResponse;
import com.assignment.powerplant.dto.response.QueryBatteryResponse;
import com.assignment.powerplant.entities.BatteryEntity;
import com.assignment.powerplant.exceptions.PowerPlantmgtExceptions;
import com.assignment.powerplant.repository.BatteryRepository;
import com.assignment.powerplant.service.BatteryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest({PowerSourceController.class})
public class PowerSourceControllerTest {
	@MockBean
    private BatteryService batteryService;

    @Mock
    private BatteryRepository batteryRepository;

    @Autowired
    private MockMvc mockMvc;
    
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }
    @Test
   public void saveBatteryDetailsTest() throws Exception {
        ResponseEntity<BatteryResponse> responseString = new ResponseEntity<>(HttpStatus.CREATED);
        Mockito.when(batteryService.create(any(BatteryRequest.class))).thenReturn(getResponseHeader());
        batteryRepository.save(new BatteryEntity());
        BatteryResponse batteryResponse = batteryService.create(getBatteryRequest());
        mockMvc.perform(
                        MockMvcRequestBuilders.post("/save/battery")
                                .content(asJsonString(getBatteryRequest()))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(content().string("All data save successfully"));

    }

    @Test
   public void getBatteryDetaisWithinRangeTest() throws Exception {
        Mockito.when(batteryService.fetchBatteryWithinRange(any(BatteryRangeRequest.class))).thenReturn(getBatteryRangeResponse());
        batteryRepository.save(new BatteryEntity());
        BatteryWithinRangeResponse batteryResponse = batteryService.fetchBatteryWithinRange(getRangeRequest());
        mockMvc.perform(
                        MockMvcRequestBuilders.post("/query/battery-range")
                                .content(asJsonString(getBatteryRequest()))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(content().string("Data successfully retrived"));
    }
    private BatteryResponse getResponseHeader() {
        BatteryResponse batteryResponse = new BatteryResponse();
        QueryBatteryResponse queryBatteryResponse = new QueryBatteryResponse();
        queryBatteryResponse.setMessage("");
        queryBatteryResponse.setDataSaveStatus(true);
        batteryResponse.setQueryBatteryResponse(queryBatteryResponse);

        return batteryResponse;
    }

    private BatteryWithinRangeResponse getBatteryRangeResponse(){
        BatteryWithinRangeResponse batteryWithinRangeResponse =new BatteryWithinRangeResponse();
        BatteryEntity batteryEntity = new BatteryEntity();
        batteryEntity.setId(56);
        batteryEntity.setCapacity(6900L);
        batteryEntity.setName("Abaca");
        batteryEntity.setPostCode("6000");
        QueryBatteryRequest queryBatteryRequest = new QueryBatteryRequest();
        List<BatteryEntity> batteryEntityList = new ArrayList<>();
        batteryEntityList.add(batteryEntity);
        queryBatteryRequest.setBatteryEntities(batteryEntityList);
        batteryWithinRangeResponse.setQueryBatteryWithinRangeResponse(queryBatteryRequest);
        return batteryWithinRangeResponse;
    }
    private BatteryRequest getBatteryRequest(){
        BatteryRequest batteryRequest = new BatteryRequest();
        BatteryEntity batteryEntity = new BatteryEntity();
        batteryEntity.setId(56);
        batteryEntity.setCapacity(6900L);
        batteryEntity.setName("");
        batteryEntity.setPostCode("5678");
        QueryBatteryRequest queryBatteryRequest = new QueryBatteryRequest();
        List<BatteryEntity> batteryEntityList = new ArrayList<>();
        batteryEntityList.add(batteryEntity);
        queryBatteryRequest.setBatteryEntities(batteryEntityList);
        batteryRequest.setQueryBatteryRequest(queryBatteryRequest);
        return  batteryRequest;
    }

    private BatteryRangeRequest getRangeRequest(){
        BatteryRangeRequest batteryRangeRequest = new BatteryRangeRequest();
        QueryBatteryRangeRequest queryBatteryRangeRequest = new QueryBatteryRangeRequest();
        queryBatteryRangeRequest.setPostCodeStart("6000");
        queryBatteryRangeRequest.setPostCodeEnd("6100");

        return  batteryRangeRequest;
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
