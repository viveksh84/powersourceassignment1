package com.assignment.powerplant;

import com.assignment.powerplant.dto.request.QueryBatteryRequest;
import com.assignment.powerplant.entities.BatteryEntity;
import com.assignment.powerplant.exceptions.PowerPlantmgtExceptions;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;


import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.assignment.powerplant.dto.request.BatteryRequest;
import com.assignment.powerplant.repository.BatteryRepository;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.assertj.core.api.Java6Assertions.assertThat;


@ExtendWith(MockitoExtension.class)
@DataJpaTest
public class PowerSourceRepositoryTest {

    @Mock
    private BatteryRepository batteryRepository;

    @BeforeEach
    void initUseCase() {

        batteryRepository.saveAll(getBatteryRequest().getQueryBatteryRequest().getBatteryEntities());
    }
    @Test
    void saveBatteryDetailsTest() throws PowerPlantmgtExceptions {
        List<BatteryEntity> customers = Arrays.asList(
                new BatteryEntity(40,"Cannington", "6107", 6500L),
                new BatteryEntity(41,"Midland", "6134", 6560L),
                new BatteryEntity(42,"Abacaa", "6108", 6800L)
        );
        Iterable<BatteryEntity> allCustomer = batteryRepository.saveAll(customers);

        AtomicInteger validIdFound = new AtomicInteger();
        allCustomer.forEach(customer -> {
            if(customer.getId()>0){
                validIdFound.getAndIncrement();
            }
        });

        assertThat(validIdFound.intValue()).isEqualTo(3);
    }

    @Test
    void getBatteryDetaisWithinRangeTest(){
        List<BatteryEntity> allCustomer = batteryRepository.findByPostCodeBetweenOrderByNameAsc("6000","6500");
        assertThat(allCustomer.size()).isEqualTo(4);
    }




    private BatteryRequest getBatteryRequest(){
        BatteryRequest batteryRequest = new BatteryRequest();
        BatteryEntity batteryEntity = new BatteryEntity();
        batteryEntity.setId(56);
        batteryEntity.setCapacity(6900L);
        batteryEntity.setName("Midas");
        batteryEntity.setPostCode("5678");
        QueryBatteryRequest queryBatteryRequest = new QueryBatteryRequest();
        List<BatteryEntity> batteryEntityList = new ArrayList<>();
        batteryEntityList.add(batteryEntity);
        queryBatteryRequest.setBatteryEntities(batteryEntityList);
        batteryRequest.setQueryBatteryRequest(queryBatteryRequest);
        return  batteryRequest;
    }
}
