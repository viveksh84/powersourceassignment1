**Powersourceassignment**

```python
This Assignment is related to virtual power plant system for aggregating distributed power sources into
a single cloud based energy provider. Please implement a REST API in spring boot that
encompasses the following functionality:
The API should have an endpoint that accepts - in the HTTP request body - a list of batteries,
each containing: name, postcode, and watt capacity. This data should be persisted in a
database (In-memory is acceptable).
The API should have an endpoint that receives a postcode range. The response body will
contain a list of names of batteries that fall within the range, sorted alphabetically. Additionally,
there should be some statistics included for the returned batteries, such as: total and average
watt capacity.
```

**Databas Used**

```python
The datbase use is MySQL and name of the DB power_plant
```

**Tools used**
```python
Spring boot versions 2.7.4
Java version 8 +
Lombock
```

**For the Unit testing service layer and repository, we are going to use the following testing libraries:**

```python
JUnit 5 Framework 
Mockito 4 (Latest) 
AssertJ Library 
DataTestJpa
```
****Testing the API ****

```python
Api end point is test using Postman
http:localhost:8080
1. Api end point for saving the battery list
    /save/battery
2. Api end point for fetching the save battery list within the postcode range provide
   /query/battery-range
```
